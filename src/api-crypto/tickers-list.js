const API_KEY =
  "d8ed30cc8bba73494b4a9993a9ab47fc88562bee4c5ff7727538e1d02cbcda4f";

const tickersList = [];

const response = fetch(
  `https://min-api.cryptocompare.com/data/all/coinlist?api_key=${API_KEY}`
);

response
  .then(res => {
    return res.json()
  })
  .then(res => {
    Object.keys(res.Data).forEach(t => tickersList.push(t));
  });

export const getTickersList = () => {
  return tickersList;
};