const API_KEY =
  "d8ed30cc8bba73494b4a9993a9ab47fc88562bee4c5ff7727538e1d02cbcda4f";
const socket = new WebSocket(
  `wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`
);

const MAIN_CURRENCY = "USD";
const SUPPORT_CURRENCY = "BTC";

const tickersHandlers = new Map(); // {}
const tickersErrorHandlers = new Map();
const tickersOnCheck = new Map();
const tmpRequestsStore = new Set();

const AGGREGATE_INDEX = "5";
const INVALID_SUB = "500";
const TOO_MANY_SOCKETS = "429";

let BTC_USD = 0;
let IS_MAIN_PAGE = false;

let INIT_RESPONSE_FROM_MAIN_PAGE = false;
const INIT_CHANNEL_REQUEST = "INIREQ";
const INIT_CHANNEL_RESPONSE = "INIRESP";
const TICKER_CHANNEL_REQUEST = "REQUEST";

// TODO [ 3 ] После удаления всех вкладок со страницы при добавлении новой запрос на websocket не уходит

const tickersChannel = new BroadcastChannel("tickers_channel");

const listenSubscribersToTickerChannel = () => {
  tickersChannel.addEventListener("message", ev => {
    if (ev.data.type === INIT_CHANNEL_REQUEST) {
      tickersChannel.postMessage({ type: INIT_CHANNEL_RESPONSE });
    }
  });
  listenRequestsFromTickerChannel();
};

const listenRequestsFromTickerChannel = () => {
  tickersChannel.addEventListener("message", ev => {
    if (IS_MAIN_PAGE && ev.data.type === INIT_CHANNEL_REQUEST) {
      subscribeToTickerOnWs(ev.data.value, MAIN_CURRENCY);
      console.log("Подписка из BroadCastChannel");
    }
    if (IS_MAIN_PAGE && ev.data.type === TICKER_CHANNEL_REQUEST) {
      console.log(ev.data.value);
      subscribeToTickerOnWs(ev.data.value.tickerName, MAIN_CURRENCY);
    }
  });
};

const listenResponsesFromTickerChannel = () => {
  tickersChannel.addEventListener("message", ev => {
    if (!IS_MAIN_PAGE && ev.data.type === "RESPONSE") {
      console.log("Слушаем ответ от сокета...");
      console.log(ev.data.value);
      const handlers = tickersHandlers.get(ev.data.value.currency) || [];
      handlers.forEach(fn => fn(ev.data.value.price));
    }
  });
};

const subscribeToTickerChannel = () => {
  tickersChannel.postMessage({ type: INIT_CHANNEL_REQUEST });
  tickersChannel.addEventListener("message", ev => {
    if (ev.data.type === INIT_CHANNEL_RESPONSE) {
			INIT_RESPONSE_FROM_MAIN_PAGE = true;
      for (const ticker of tmpRequestsStore) {
        tickersChannel.postMessage({ type: TICKER_CHANNEL_REQUEST, value: ticker });
      }
			tmpRequestsStore.clear();
      listenResponsesFromTickerChannel();
    }
  });
};

const sendRequestToTickerChannel = tickerName => {
  tickersChannel.postMessage({ type: TICKER_CHANNEL_REQUEST, value: { tickerName } });
};

socket.addEventListener("message", e => {
  const {
    TYPE: type,
    FROMSYMBOL: currency,
    PRICE: newPrice,
    PARAMETER: param,
    MESSAGE: message
  } = JSON.parse(e.data);

  if (type === INVALID_SUB && message === "INVALID_SUB") {
    handleInvalidSub(param);
  }

  if (type === TOO_MANY_SOCKETS) {
    console.log("Сокет уже занят...");
    subscribeToTickerChannel();
    return;
  }

  if (type !== AGGREGATE_INDEX || newPrice === undefined) {
    return;
  }

  IS_MAIN_PAGE = true;
  listenSubscribersToTickerChannel();

	console.log("I am a main page !!!")

  if (tickersOnCheck.has(currency)) {
    updateTickersOnSupportList(currency, newPrice);
  }

  if (currency === "BTC" && tickersOnCheck.size !== 0) {
    updateTickersOnSupport(newPrice);
  }

  const price = tickersOnCheck.has(currency) ? newPrice * BTC_USD : newPrice;

  const handlers = tickersHandlers.get(currency) ?? [];

  tickersChannel.postMessage({ type: "RESPONSE", value: { currency, price } });

  handlers.forEach(fn => fn(price));
});

function updateTickersOnSupport(newPrice) {
  BTC_USD = newPrice;
  for (const [key, value] of tickersOnCheck.entries()) {
    const handlers = tickersHandlers.get(key) ?? [];
    const price = value * BTC_USD;
    handlers.forEach(fn => fn(price));
  }
}

function updateTickersOnSupportList(currency, newPrice) {
  if (BTC_USD === 0) {
    subscribeToTickerOnWs(SUPPORT_CURRENCY, MAIN_CURRENCY);
  }
  tickersOnCheck.set(currency, newPrice);
}

function handleInvalidSub(param) {
  const currency = param.split("~")[2];

  if (tickersOnCheck.has(currency)) {
    tickersOnCheck.delete(currency);
    const handlers = tickersErrorHandlers.get(currency) ?? [];
    handlers.forEach(fn => fn(currency));
    return;
  } else {
    subscribeToTickerOnWs(currency, SUPPORT_CURRENCY);
    tickersOnCheck.set(currency, 0);
    return;
  }
}

function sendToWebSocket(message) {
  const stringifiedMessage = JSON.stringify(message);

  if (socket.readyState === WebSocket.OPEN) {
    socket.send(stringifiedMessage);
    return;
  }

  socket.addEventListener(
    "open",
    () => {
      socket.send(stringifiedMessage);
    },
    { once: true }
  );
}

function subscribeToTickerOnWs(tickerName, currency) {
  if (tickerName === SUPPORT_CURRENCY && BTC_USD !== 0) return;
  sendToWebSocket({
    action: "SubAdd",
    subs: [`5~CCCAGG~${tickerName}~${currency}`]
  });
}

function unsubscribeFromTickerOnWs(tickerName, currency) {
  if (tickerName === SUPPORT_CURRENCY && tickersOnCheck.size !== 0) return;
  if (!tickersHandlers.has(SUPPORT_CURRENCY) && tickersOnCheck.size === 0) {
    BTC_USD = 0;
    sendToWebSocket({
      action: "SubRemove",
      subs: [`5~CCCAGG~${SUPPORT_CURRENCY}~${MAIN_CURRENCY}`]
    });
  }
  sendToWebSocket({
    action: "SubRemove",
    subs: [`5~CCCAGG~${tickerName}~${currency}`]
  });
  console.log({ BTC_USD });
}

export const subscribeOnTickerChecking = (ticker, cb) => {
  const subscribers = tickersErrorHandlers.get(ticker) || [];
  tickersErrorHandlers.set(ticker, [...subscribers, cb]);
};

export const subscribeToTicker = (tickerName, cb) => {
  const subscribers = tickersHandlers.get(tickerName) || [];
  tickersHandlers.set(tickerName, [...subscribers, cb]);
  if (!IS_MAIN_PAGE) {
		if (!INIT_RESPONSE_FROM_MAIN_PAGE) {
			subscribeToTickerOnWs(tickerName, MAIN_CURRENCY);
			tmpRequestsStore.add(tickerName);
			return;
		}
    sendRequestToTickerChannel(tickerName);
    listenResponsesFromTickerChannel();
  }

  subscribeToTickerOnWs(tickerName, MAIN_CURRENCY);
};

export const unsubscribeFromTicker = tickerName => {
  tickersHandlers.delete(tickerName);
  if (tickersOnCheck.has(tickerName)) {
    tickersOnCheck.delete(tickerName);
    unsubscribeFromTickerOnWs(tickerName, SUPPORT_CURRENCY);
    return;
  }
  unsubscribeFromTickerOnWs(tickerName, MAIN_CURRENCY);
};
